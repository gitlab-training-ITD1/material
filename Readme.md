## General Information
<b>Branch | Information Technology
:--|:--|
<b> Lab | A/B/C
<b>Mentor Name|      Prof. Priyanka Buch

#### About


<b>Name  | Riddhi Sherasiya
:--|:--|
<b> Institute |  Marwadi University
<b> Email id|      sherasiyariddhi18@gmail.com
<b> Department | Information Technology

#### Enter your friend's detail

<b>SrNo | <b>Name | <b>Faculty or Student | <b>Department| <b>Institute | <b>Email id
:--|:--|:--|:--|:--|:--|
1 | Shaiva Bhalani | Student | Information Technology | Marwadi University,Rajkot | shaiva.bhalani11304@marwadieducation.edu.in
2 | Nidhi Hinsu | Student | Information Technology | Marwadi University,Rajkot | nidhihinsu@gmail.com
